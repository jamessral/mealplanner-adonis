'use strict'

const { validate } = use('Validator')
const Encryption = use('Encryption')
const User = use('App/Models/User')
const Token = use('App/Models/Token')

class AuthController {
  async signIn ({ request, response, auth }) {
    const rules = {
      email: 'required|email',
      password: 'required'
    }

    const { email, password } = request.only(['email', 'password'])

    const validation = await validate({ email, password }, rules)

    if (!validation.fails()) {
      try {
        return await auth.withRefreshToken().attempt(email, password)
      } catch (err) {
        response.status(401).send({ error: 'Invalid email or password' })
      }
    } else {
      response.status(401).send(validation.messages())
    }
  }

  async register ({ request, response }) {
    const rules = {
      email: 'required|email|unique:users,email',
      password: 'required'
    }

    const { email, password } = request.only([
      'email',
      'password'
    ])

    const validation = await validate({ email, password }, rules)

    if (!validation.fails()) {
      try {
        const user = await User.create({ email, password })
        if (user) {
          return response.send({ message: 'User has been created' })
        }
        throw new Error('Could not create user')
      } catch (err) {
        response.status(401).send({ error: 'Please try again' })
      }
    } else {
      response.status(401).send(validation.messages())
    }
  }

  async refreshToken ({ request, response, auth }) {
    const rules = {
      refresh_token: 'required'
    }

    const { refreshToken } = request.only(['refresh_token'])

    const validation = await validate({ refresh_token: refreshToken }, rules)

    if (!validation.fails()) {
      try {
        return await auth
          .newRefreshToken()
          .generateForRefreshToken(refreshToken)
      } catch (err) {
        response.status(401).send({ error: 'Invalid refresh token' })
      }
    } else {
      response.status(401).send(validation.messages())
    }
  }

  async logout ({ request, response, auth }) {
    const rules = {
      refresh_token: 'required'
    }

    const { refreshToken } = request.only(['refresh_token'])

    const validation = await validate({ refresh_token: refreshToken }, rules)

    const decrypted = Encryption.decrypt(refreshToken)

    if (!validation.fails()) {
      try {
        const refreshToken = await Token.findBy('token', decrypted)
        if (refreshToken) {
          refreshToken.delete()
          response.status(200).send({ status: 'ok' })
        } else {
          response.status(401).send({ error: 'Invalid refresh token' })
        }
      } catch (err) {
        response.status(401).send({ error: 'something went wrong' })
      }
    } else {
      response.status(401).send(validation.messages())
    }
  }
}

module.exports = AuthController
