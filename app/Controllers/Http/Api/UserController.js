'use strict'

class UserController {
  async getUser ({ response, auth }) {
    try {
      const { email } = await auth.getUser()
      return { email }
    } catch (error) {
      response.send('Missing or invalid jwt token')
    }
  }
}

module.exports = UserController
