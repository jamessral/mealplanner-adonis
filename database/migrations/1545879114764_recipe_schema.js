'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RecipeSchema extends Schema {
  up () {
    this.create('recipes', (table) => {
      table.increments()
      table.string('name', 64).notNullable().index()
      table.text('description')
      table.integer('user_id').unsigned().notNullable().index()
      table.foreign('user_id').references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('recipes')
  }
}

module.exports = RecipeSchema
