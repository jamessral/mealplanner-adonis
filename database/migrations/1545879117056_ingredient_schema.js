'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class IngredientSchema extends Schema {
  up () {
    this.create('ingredients', (table) => {
      table.increments()
      table.string('name', 64).notNullable().index()
      table.text('description')
      table.decimal('price').notNullable().defaultTo(0).index()
      table.string('unit', 64).notNullable()
      table.integer('quantity', 64).unsigned().notNullable()
      table.integer('user_id').unsigned().notNullable().index()
      table.foreign('user_id').references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('ingredients')
  }
}

module.exports = IngredientSchema
