'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class IngredientsRecipesSchema extends Schema {
  up () {
    this.create('ingredients_recipes', (table) => {
      table.increments()
      table.integer('ingredient_id').unsigned().notNullable()
      table.foreign('ingredient_id').references('id').inTable('ingredients')
      table.integer('recipe_id').unsigned().notNullable()
      table.foreign('recipe_id').references('id').inTable('recipes')
      table.timestamps()
    })
  }

  down () {
    this.drop('ingredients_recipes')
  }
}

module.exports = IngredientsRecipesSchema
