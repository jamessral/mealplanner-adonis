'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
Route.group(() => {
  Route.post('/auth/sign_in', 'Api/AuthController.signIn')
  Route.post('/auth/sign_up', 'Api/AuthController.register')
  Route.post('/auth/token/refresh', 'Api/AuthController.refreshToken')
  Route.post('/auth/logout', 'Api/AuthController.logout')

  Route.get('/user/profile', 'Api/UserController.getUser').middleware('auth:jwt')

  Route.get('/ingredients', 'Api/IngredientController.index').middleware('auth:jwt')
}).prefix('/api/v1')
